﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiSwagger.Models;

namespace WebApiSwagger.Controllers
{
    [RoutePrefix("api/Estudiantes")]
    public class EstudiantesController : ApiController
    {

        private static List<Student> StudentsList;

        public EstudiantesController()
        {
            if (StudentsList == null)
            {
                StudentsList = StudentsData.CreateStudents();
            }

        }

        /// <summary>
        /// Obtengo todos los Estudiantes
        /// </summary>
        /// <remarks>Obtengo un Arreglo con todos los Estudiantes</remarks>
        /// <response code="500">Internal Server Error</response>
        [Route("")]
        [ResponseType(typeof(List<Student>))]
        public IHttpActionResult Get()
        {
            return Ok(StudentsList);
        }

        /// <summary>
        /// Obtengo Estudiante
        /// </summary>
        /// <param name="userName">Unique username</param>
        /// <remarks>Obtengo la info de un estudiante por medio de su username</remarks>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [Route("{userName:alpha}", Name = "GetStudentByUserName")]
        [ResponseType(typeof(Student))]
        public IHttpActionResult Get(string userName)
        {

            var student = StudentsList.Where(s => s.UserName == userName).FirstOrDefault();

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        /// <summary>
        /// Agrego Nuevo Estudiantet
        /// </summary>
        /// <param name="student">Objeto Estudiante</param>
        /// <remarks>Agrego un Nuevo Estudiante</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("")]
        [ResponseType(typeof(Student))]
        public IHttpActionResult Post(Student student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (StudentsList.Any(s => s.UserName == student.UserName))
            {
                return BadRequest("Usuario ya existe");
            }

            StudentsList.Add(student);

            string uri = Url.Link("GetStudentByUserName", new { userName = student.UserName });

            return Created(uri, student);
        }

        /// <summary>
        /// Elimino Estudiante
        /// </summary>
        /// <param name="userName">Unique username</param>
        /// <remarks>Elimina un estudiante por medio de su userName</remarks>
        /// <response code="404">Not found</response>
        /// <response code="500">Internal Server Error</response>
        [Route("{userName:alpha}")]
        public HttpResponseMessage Delete(string userName)
        {

            var student = StudentsList.Where(s => s.UserName == userName).FirstOrDefault();

            if (student == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            StudentsList.Remove(student);

            return Request.CreateResponse(HttpStatusCode.NoContent);

        }

    }

    public class StudentsData
    {
        public static List<Student> CreateStudents()
        {

            List<Student> studentsList = new List<Student>();

            for (int i = 0; i < studentNames.Length; i++)
            {
                var nameGenderMail = SplitValue(studentNames[i]);
                var student = new Student()
                {
                    Email = String.Format("{0}.{1}@{2}", nameGenderMail[0], nameGenderMail[1], nameGenderMail[3]),
                    UserName = String.Format("{0}{1}", nameGenderMail[0], nameGenderMail[1]),
                    FirstName = nameGenderMail[0],
                    LastName = nameGenderMail[1],
                    DateOfBirth = DateTime.UtcNow.AddDays(-new Random().Next(7000, 8000)),
                };

                studentsList.Add(student);
            }

            return studentsList;
        }

        static string[] studentNames = 
        { 
            "Robert,Rozas,Masculino,hotmail.com", 
            "Gabriel,Gonzalez,Masculino,mymail.com", 
            "Daniel,Toledo,Masculino,outlook.com", 
            "Katherine,Rozas,Femenino,outlook.com"            
        };

        private static string[] SplitValue(string val)
        {
            return val.Split(',');
        }
    }
}
